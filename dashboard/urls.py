"""dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URLw to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from datetime import datetime

from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views
from django.conf.urls import include, url
from django.contrib.auth.views import HttpResponseRedirect

from app import views as bing_views
from app.forms import BootstrapAuthenticationForm
from googleauth import views as google_views
from adsdashboard import views as dash_views

admin.autodiscover()

urlpatterns = [
    path('', include('django_plotly_dash.urls'), name='dashboard'),
    path('', dash_views.session_state_view, {'template_name':'dashboard.html'},
                                             name="dashboard_page"),
    path('bing/login',            bing_views.callback, name='binglogin'),
    path('bing/login/authorized', bing_views.callback, name='bingauthorized'),
    path('bing/logout',           bing_views.applogout, name='binglogout'),
    path('bing/revoke',           bing_views.revoke, name='bingrevoke'),
    path('google/login',          google_views.login_redirect, name='googlelogin'),
    path('google/login/authorized', google_views.authorized, name='googleauthorized'),
    path('app/',                  bing_views.home, name='home'),
    path('login/',
        auth_views.LoginView.as_view(
            template_name='app/login.html',
            authentication_form=BootstrapAuthenticationForm,
            extra_context= {
                'title':'Log in',
                'year':datetime.now().year,
            }
        ),
        name='login'),
    path('logout/',
        auth_views.LogoutView.as_view(),
        {
            'next_page': '/',
        },
        name='logout'),

    path('admin/', admin.site.urls),
]
