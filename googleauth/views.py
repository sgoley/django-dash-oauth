from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
import google.oauth2.credentials
import google_auth_oauthlib.flow
from googleads import oauth2
from googleads import adwords

from googleauth.models import GoogleUser


flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
    'googleauth/client_secret_915202269687-ts7261dh7a92rtg3m186g1a54h555k9s.apps.googleusercontent.com.json',
    scopes=[oauth2.GetAPIScope('adwords')])
# Indicate where the API server will redirect the user after the user completes
# the authorization flow. The redirect URI is required.
flow.redirect_uri = 'https://djangodashoauth.herokuapp.com/google/login/authorized'


def login_redirect(request):
    # Initialize the flow using the client ID and secret downloaded earlier.
    # Note: You can use the GetAPIScope helper function to retrieve the
    # appropriate scope for AdWords or Ad Manager.
    # Enable offline access so that you can refresh an access token without
    # re-prompting the user for permission. Recommended for web server apps.
    authorization_url, state = flow.authorization_url(access_type='offline',)
    # # Enable incremental authorization. Recommended as a best practice.
    # include_granted_scopes='true')

    return redirect(authorization_url)


def authorized(request):

    try:
        user = User.objects.get(username=request.user.username)
        googleadwordsuser = GoogleUser()
        googleadwordsuser.user = user
        # googleadwordsuser = user.googleadwordsuser
    # except User.DoesNotExist:
    #     return render(
    #         request,
    #         'app/index.html'
    #     )
    except GoogleUser.DoesNotExist:
        pass

    auth_code = request.GET.get('code', '')
    flow.fetch_token(code=auth_code)
    credentials = flow.credentials
    print(auth_code, flow, credentials)
    print(credentials.refresh_token)
    googleadwordsuser.refresh_token = credentials.refresh_token

    user.save()
    googleadwordsuser.save()
    # ['apply', 'before_request', 'expired', 'expiry', 'from_authorized_user_file', 'from_authorized_user_info', 'has_scopes', 'id_token', 'refresh', , 'token_uri', 'valid'
    # cliend_id, client_secret, refresh_token, token = (credentials.client_id,
    #                                                   credentials.client_secret,
    #                                                   credentials.refresh_token,
    #                                                   credentials.token)
    #
    #
    oauth2_client = oauth2.GoogleRefreshTokenClient(credentials.client_id, credentials.client_secret, credentials.refresh_token)
    print(dir(oauth2_client))
    return redirect('dashboard_page')




def get_adwords_client():
    # Initialize the GoogleRefreshTokenClient using the credentials you received
    # in the earlier steps.

    # Initialize the AdWords client.
    adwords_client = adwords.AdWordsClient(
        developer_token, oauth2_client, user_agent, client_customer_id=client_customer_id)
    return
