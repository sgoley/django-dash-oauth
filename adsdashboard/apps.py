from django.apps import AppConfig


class AdsdashboardConfig(AppConfig):
    name = 'adsdashboard'
