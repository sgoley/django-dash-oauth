from django.shortcuts import render

def session_state_view(request, template_name="dashboard.html", **kwargs):

    # Set up a context dict here
    context = {} #{ ... values for template go here, see below ... }

    return render(request, template_name=template_name, context=context)
