from django.shortcuts import redirect
import dash
import dash_core_components as dcc
import dash_html_components as html

from django_plotly_dash import DjangoDash


app = DjangoDash('SimpleExample')   # replaces dash.Dash

app.layout = html.Div([
    html.Button(html.A("Bing login new window", href='/bing/login', target="_blank")),
    html.Button(html.A("Bing login this window", href='/bing/login', target="_parent")),
    html.Br(),
    html.Button(html.A("Goolge login new window", href='/google/login', target="_blank")),
    html.Button(html.A("Goolge login this window", href='/google/login', target="_parent")),

    html.Br(),
    html.Button("Check bing auth", id='check-bing-auth'),
    html.Div(id='bing-auth-output'),
    html.Button("Check google auth", id='check-google-auth'),
    html.Div(id='google-auth-output')

])

@app.expanded_callback(dash.dependencies.Output('bing-auth-output', 'children'),
                      [dash.dependencies.Input('check-bing-auth', 'n_clicks')])
def bing_authorized_check(dropdown_value, dash_app, dash_app_id, session_state, user):
    from app.views import get_user, search_accounts_by_user_id, customer_service
    bing_ads_user = customer_service.GetUser(UserId = None).User
    return str(search_accounts_by_user_id(bing_ads_user.Id)['AdvertiserAccount'])


@app.expanded_callback(dash.dependencies.Output('google-auth-output', 'children'),
                      [dash.dependencies.Input('check-google-auth', 'n_clicks')])
def google_authorized_check(dropdown_value, dash_app, dash_app_id, session_state, user):
    # from app.views import get_user, search_accounts_by_user_id, customer_service
    # bing_ads_user = customer_service.GetUser(UserId = user_id).User
    return
